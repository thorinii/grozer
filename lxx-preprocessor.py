import json
import re
import sys
import xml.sax
from xml.sax.handler import ContentHandler

from greek_normalisation.normalise import Normaliser, Norm
from greek_normalisation.utils import nfd


normaliser = Normaliser(config=(
    Norm.GRAVE |
    Norm.ELISION |
    Norm.MOVABLE |
    Norm.EXTRA |
    Norm.PROCLITIC |
    Norm.ENCLITIC |
    Norm.NO_ACCENT
)).normalise


def main(args):
    if not args:
        return

    with open(args[0], 'r') as fp:
        xml.sax.parse(fp, LxxSaxHandler(LxxWordSlicer()))


def normalise(text, lower=False):
    text, _ = normaliser(text)
    if lower:
        text = text.lower()
    return nfd(text)


class LxxSaxHandler(ContentHandler):
    def __init__(self, word_slicer):
        self.word_slicer = word_slicer

        self.indent = 0
        self.stack = []
        self.chapter = None
        self.verse = None

    def startElement(self, name, attrs):
        self.indent += 1
        self.stack.append(name)
        if len(self.stack) > 2 and self.stack[1] == "text":
            if name == "div" and "type" in attrs and attrs["type"] == "textpart":
                if attrs["subtype"] == "chapter":
                    self.chapter = attrs["n"]
                    self.verse = 1
                elif attrs["subtype"] == "verse":
                    self.verse = attrs["n"]

    def endElement(self, name):
        self.indent -= 1
        self.stack.pop()

    def characters(self, text):
        text = text.strip()
        if not text:
            return
        if "note" in self.stack:
            return
        if "p" not in self.stack and "l" not in self.stack:
            return
        if len(self.stack) > 2 and self.stack[1] == "text":
            self.word_slicer.take(self.chapter, f"{self.chapter}:{self.verse}", text)

    def endDocument(self):
        self.word_slicer.finish()


TOKEN_NORMALISE_REGEX = re.compile(r"[^\u00A8\u00B4\u00B7\u02BC\u0300-\u0362\u0370-\u03FF]")
ONLY_GREEK_REGEX = re.compile(r"[\u00A8\u00B4\u00B7\u02BC\u0300-\u0362\u0370-\u03FF\u1FBD,.:;'\"-]+")
PUNCTUATED_SPACE = re.compile(r" +([-;:,.])")
UNSPACED_PUNCTUATION = re.compile(r"(\S)([-])(\S)")
NUMERIC_COUNTERS = re.compile(r"\(?[0-9]+\)?")
LETTER_COUNTERS = re.compile(r"[a-zᵃᵇ]+")
ROMAN_NUMERALS = re.compile(r"[IVXLC]+")
ENGLISH_LETTERS = re.compile(r"[A-Za-z]+")
UPPER_LETTER_COUNTERS = re.compile(r"(^|[^A-Z])[A-Z]($|[^A-Z])")
BRACKET_SECTIONS = re.compile(r"\[[^\]]+\]")


class LxxWordSlicer:
    def __init__(self):
        self.current_text = ""
        self.current_phrase = None

    def take(self, section, phrase, text):
        if self.current_phrase != phrase:
            if self.current_phrase:
                self._process(self.current_phrase, self.current_text)
            self.current_phrase = phrase
            self.current_text = ""
        self.current_text += " " + text

    def finish(self):
        if self.current_phrase:
            self._process(self.current_phrase, self.current_text)
            self.current_phrase = None

    def _process(self, phrase, text):
        text = normalise(text)

        # remove bracketted sections
        text = BRACKET_SECTIONS.sub("", text)

        # fix Greek punctuation marks
        text = (text
            .replace("•", "·")
            .replace("—", "--")
            .replace("‘", "\u0314")
        )

        # remove bad punctation marks
        text = (text
            .replace("??", "")
            .replace("»", ". ")
            .replace("§", "")
            .replace("¶", "")
            .replace("¶", "")
            .replace("°", "")
            .replace("\u05B9", "")
            .replace("\u00A0", " ")
            .replace("\u0081", "")
            .replace("\u008D", "")
            .replace("\u009A", "")
        )

        # remove numbered markers
        text = (text
            .replace("¹", "")
            .replace("²", "")
            .replace("³", "")
            .replace("⁴", "")
            .replace("⁵", "")
            .replace("⁶", "")
            .replace("⁷", "")
            .replace("⁸", "")
            .replace("⁹", "")
            .replace("⁰", "")
        )

        # normalise quotes
        text = (text
            .replace("᾿", '᾽')
            .replace("῾", '"')
            .replace("’", '"')
            .replace("‟", '"')
        )

        # remove pure English
        text = UPPER_LETTER_COUNTERS.sub("", text)

        text = NUMERIC_COUNTERS.sub("", text)
        text = LETTER_COUNTERS.sub("", text)

        for word in text.split(' '):
            word = word.strip()
            if not word:
                continue

            if word == "αὐτ£√":
                word = "αὐτοῦ"

            if ROMAN_NUMERALS.fullmatch(word):
                continue

            if all(c in "ABEHIKMNOPTXYZ" for c in word):
                print(f"probable Greek uppercase {phrase}: {word}", file=sys.stderr)
                word = (word
                    .replace("A", "Α")
                    .replace("B", "Β")
                    .replace("E", "Ε")
                    .replace("H", "Η")
                    .replace("I", "Ι")
                    .replace("K", "Κ")
                    .replace("M", "Μ")
                    .replace("N", "Ν")
                    .replace("O", "Ο")
                    .replace("P", "Ρ")
                    .replace("T", "Τ")
                    .replace("X", "Χ")
                    .replace("Y", "Υ")
                    .replace("Z", "Ζ")
                )

            word = ENGLISH_LETTERS.sub("", word)
            word = (word
                .replace("?", "")
                .replace("(", "")
                .replace(")", "")
                .replace("[", "")
                .replace("]", "")
                .replace("*", "")
                .replace("/", "")
                .replace("\\", "")
                .replace("^", "")
                .replace("+", "")
                .replace(">", "")
                .replace("|", "")
                .replace("\t", "")
            )
            if not word:
                continue

            if not ONLY_GREEK_REGEX.fullmatch(word):
                print(f"bad input {phrase}: {word} => {text}", file=sys.stderr)
                print(f"  {len(word)} " + ", ".join([f"'{s}'" for s in ONLY_GREEK_REGEX.split(word)]), file=sys.stderr)
                continue

            token = TOKEN_NORMALISE_REGEX.sub("", word.lower())
            if token == "":
                continue

            print((phrase, word, token))


main(sys.argv[1:])
