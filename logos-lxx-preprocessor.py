from bs4 import BeautifulSoup
import json
import re
import sqlite3
import sys
from typing import Iterable, NamedTuple, Optional

from greek_normalisation.normalise import Normaliser, Norm
from greek_normalisation.utils import nfkc


class Word(NamedTuple):
    token: str
    form: str
    lemma: str
    translation: str
    trailing_punctuation: Optional[str]

    ends_sentence: bool
    ends_paragraph: bool

    chapter: int
    verse: int


normaliser = Normaliser(
    config=(
        Norm.GRAVE
        | Norm.ELISION
        | Norm.MOVABLE
        | Norm.EXTRA
        | Norm.PROCLITIC
        | Norm.ENCLITIC
        | Norm.NO_ACCENT
    )
).normalise

ONLY_NUMBERS = re.compile(r"^[0-9]+$")
BAD_PUNCTUATION = {"⌜", "⌝"}


def main(args):
    if not args or len(args) < 4:
        print(f"Usage: python {__file__} <db file> <book id> <book name> <htmls>...")
        return
    db_file, bookid, book_name, *htmls = args

    print("Processing...")
    words = process_into_words(htmls)

    print(f"Importing...")
    import_into_db(db_file, bookid, book_name, words)


def process_into_words(htmls):
    fragments = chop_up_htmls(htmls)
    tokens = collect_tokens(fragments)
    tokens = filter_bad_punctuation(tokens)
    tokens = collect_punctuation(tokens)
    tokens = invert_paragraph_markers(tokens)
    return list(count_metrics(tokens))


def chop_up_htmls(htmls):
    for html_file in htmls:
        print(f"Reading {html_file}...")
        with open(html_file, "r") as fp:
            soup = BeautifulSoup(fp, "html.parser")

        paragraphs = extract_paragraphs(soup)
        yield from extract_token_fragments(paragraphs)


def extract_paragraphs(soup: BeautifulSoup):
    for p in soup.find_all("p"):
        if p.get("lang") != "en-US":
            continue
        yield p


def extract_token_fragments(paragraphs):
    for p in paragraphs:
        for span in p.find_all("span"):
            style = span.get("style")
            lang = span.get("lang")
            text = span.text.strip()
            if not text:
                continue

            if style == "font-size:16pt;":
                # find chapter markers
                yield ("chapter", text)
            elif span.find("sup"):
                # find verse markers
                yield ("verse", text)
            elif lang == "el":
                # find greek stuff
                text = normalise(text)
                text = text.replace("ʼ", " ̓")

                if "font-size:11pt" in style:
                    yield ("lemma", text)
                else:
                    yield ("token", text)
            elif style == "font-size:10pt;":
                # find usable english
                if ONLY_NUMBERS.match(text):
                    # don't want the notes
                    pass
                else:
                    yield ("translation", text)
            else:
                # everything else is noise
                pass
        yield ("paragraph", None)


def collect_tokens(fragments):
    token = None
    lemma = None
    translation = None

    for f in fragments:
        kind, value = f

        if kind == "token":
            if token and lemma and translation:
                yield ("token", token, lemma, translation)
            elif token:
                yield ("punctuation", token)

            token = value
            lemma = translation = None
        elif kind == "lemma":
            lemma = value
        elif kind == "translation":
            translation = value
        else:
            if token and lemma and translation:
                yield ("token", token, lemma, translation)
            elif token:
                yield ("punctuation", token)
            token = lemma = translation = None
            yield f

    if token and lemma and translation:
        yield ("token", token, lemma, translation)
    elif token:
        yield ("punctuation", token)


def filter_bad_punctuation(tokens):
    for t in tokens:
        if t[0] == "punctuation":
            if t[1] in BAD_PUNCTUATION:
                continue
        yield t


def collect_punctuation(tokens):
    previous = None
    for t in tokens:
        kind = t[0]
        if kind == "punctuation":
            if previous:
                yield (*previous, t[1])
                previous = None
            else:
                # drop it
                pass
        else:
            if previous:
                yield (*previous, None)
                previous = None

            if kind == "token":
                previous = t
            else:
                yield t

    if previous:
        yield (*previous, None)


def invert_paragraph_markers(tokens):
    """
    Changes the meaning of paragraph markers from signalling the beginning of a
    new paragraph to signalling the end of the previous.
    """

    previous_token = None
    for token in tokens:
        kind = token[0]

        if kind == "paragraph":
            if previous_token:
                yield token
                yield previous_token
                previous_token = None
        elif kind == "token":
            if previous_token:
                yield previous_token
            previous_token = token
        else:
            yield token


def count_metrics(tokens) -> Iterable[Word]:
    chapter = None
    verse = None
    ends_paragraph = False

    for token in tokens:
        kind = token[0]

        if kind == "chapter":
            chapter = token[1]
            verse = None
        elif kind == "verse":
            verse = token[1]
        elif kind == "paragraph":
            ends_paragraph = True
        elif kind == "token":
            _, word, lemma, translation, punctuation = token
            yield Word(
                token=word,
                form=normalise(word, lower=True),
                lemma=lemma,
                translation=translation,
                trailing_punctuation=punctuation,
                ends_sentence=punctuation in (".", ";", "·"),
                ends_paragraph=ends_paragraph,
                chapter=chapter,
                verse=verse,
            )

            ends_paragraph = False
        else:
            raise Exception(f"Unknown token: {kind}")


def normalise(text, lower=False):
    text, _ = normaliser(text)
    if lower:
        text = text.lower()
    return nfkc(text)


def import_into_db(db_file, bookid, book_name, words):
    with sqlite3.connect(db_file) as db:
        db.execute("BEGIN;")
        clean_by_book(db, bookid)
        insert_forms(db, words)
        word_ids = insert_words(db, bookid, words)
        insert_sentences(db, bookid, words, word_ids)
        insert_texts(db, bookid, book_name, words)
        update_sentence_ids(db, bookid)
        recalculate_form_frequency(db)
        db.commit()


def clean_by_book(db, bookid):
    db.execute(
        """
        DELETE FROM text
        WHERE text.id IN (SELECT text_id FROM word WHERE book = ?);
        """,
        (bookid,),
    )
    db.execute(
        "DELETE FROM word WHERE book = ?;",
        (bookid,),
    )
    db.execute(
        "DELETE FROM sentence WHERE book = ?;",
        (bookid,),
    )
    db.execute(
        """
        DELETE FROM form
        WHERE form.form NOT IN (SELECT form FROM word);
        """,
    )


def insert_words(db, bookid, words):
    cursor = db.execute("SELECT MAX(id) FROM word;")
    next_word_id = (cursor.fetchone() or (0,))[0] + 1

    print(f"Inserting {len(words)} words...")
    db.executemany(
        """
        INSERT INTO word (id, book, chapter, verse,
                          word, form, lexeme, interlinear, study,
                          sentence, paragraph, punctuation)
        VALUES (?, ?, ?, ?,
                ?, ?, ?, ?, ?,
                ?, ?, ?);
        """,
        (
            (
                idx + next_word_id,
                bookid,
                w.chapter,
                w.verse,
                w.token,
                w.form,
                w.lemma,
                w.translation,
                w.translation,
                w.ends_sentence,
                w.ends_paragraph,
                w.trailing_punctuation or "",
            )
            for idx, w in enumerate(words)
        ),
    )

    return [i + next_word_id for i in range(len(words))]


def insert_sentences(db, bookid, words: Iterable[Word], word_ids):
    # chunk words into sentences
    start_id = None
    start_word = None
    sentence_words = None
    sentence_translation = None
    sentences = []
    for id, w in zip(word_ids, words):
        if not start_id:
            start_id = id
            start_word = w
            sentence_words = []
            sentence_translation = []

        sentence_words.append(w.token + (w.trailing_punctuation or ""))
        if w.translation != "-":
            sentence_translation.append(w.translation)

        if w.ends_sentence or w.ends_paragraph:
            if start_id:
                sentences.append(
                    (
                        start_id,
                        id,
                        start_word,
                        " ".join(sentence_words),
                        " ".join(sentence_translation),
                    )
                )
                start_id = None

    if start_id:
        sentences.append(
            (
                start_id,
                id,
                start_word,
                " ".join(sentence_words),
                " ".join(sentence_translation),
            )
        )

    cursor = db.execute("SELECT MAX(id) FROM sentence;")
    next_sentence_id = (cursor.fetchone() or (0,))[0] + 1

    print(f"Inserting {len(sentences)} sentences...")
    db.executemany(
        """
        INSERT INTO sentence (id, start_word, end_word,
                              book, chapter, verse,
                              text, translation)
        VALUES (?, ?, ?,
                ?, ?, ?,
                ?, ?);
        """,
        (
            (
                next_sentence_id + idx,
                s[0],
                s[1],
                bookid,
                s[2].chapter,
                s[2].verse,
                s[3],
                s[4],
            )
            for idx, s in enumerate(sentences)
        ),
    )


def insert_forms(db, words):
    forms = list(set(word.form for word in words))
    forms.sort()

    print(f"Inserting {len(forms)} forms...")
    db.executemany(
        "INSERT OR IGNORE INTO form (form) VALUES (?);",
        [(f,) for f in forms],
    )


def insert_texts(db, bookid, book_name, words):
    chapters = list(set(int(word.chapter) for word in words))

    print(f"Inserting {len(chapters)} texts...")
    db.executemany(
        """
        INSERT INTO text (id, title, enabled)
        VALUES (? * 100 + ?, 'LXX ' || ? || ' ' || ?, 1);
        """,
        [(bookid, c, book_name, c) for c in chapters],
    )

    db.execute(
        """
        UPDATE word SET text_id = word.book * 100 + word.chapter;
        """
    )
    db.execute(
        """
        UPDATE sentence SET text_id = sentence.book * 100 + sentence.chapter;
        """
    )


def update_sentence_ids(db, bookid):
    print("Updating sentence ids on words...")
    db.execute(
        """
        UPDATE word SET sentence_id = (SELECT s.id FROM sentence s
                                       WHERE word.id >= s.start_word
                                         AND word.id <= s.end_word)
        WHERE book = ?;
        """,
        (bookid,),
    )


def recalculate_form_frequency(db):
    print("Recalculating form frequencies...")
    db.execute(
        """
        UPDATE form SET frequency = (SELECT COUNT(*) FROM word
                                     WHERE word.form = form.form);
        """
    )


main(sys.argv[1:])
