from datetime import datetime, timedelta


def main():
    ITERATIONS = 5000
    NEW_CARD_RATE = 50
    INITIAL_PERIOD = 2

    cards = []
    now = datetime.now()
    maximum_review = 0

    for day_index in range(0, ITERATIONS):
        date = now + timedelta(days=day_index)

        if date.weekday() in (0, 1, 2, 4):
            for _ in range(0, NEW_CARD_RATE):
                cards.append((day_index, INITIAL_PERIOD))

        total_count = len(cards)
        review_count = len([c for c in cards if c[0] <= day_index])
        maximum_review = max(maximum_review, review_count)
        if day_index % 30 == 0:
            print(f'{date:%Y-%m-%d}: {review_count:3d} / {total_count:3d}')
            print(f'            Worst review day: {maximum_review}')

        if date.weekday() in (0, 1, 2, 4):
            cards = [execute(day_index, c) for c in cards]

        if total_count > 17700:
            break

    print(f'Worst review day: {maximum_review}')


def execute(day_index, card):
    appear, period = card
    if appear > day_index:
        return appear, period
    else:
        return day_index + period, period * 2


main()
