from datetime import datetime, timedelta
import json
import math
import os
import random
import sqlite3

import bottle
import pytz


PRACTICE_LIMIT = 10
SHORT_READER_SHUFFLE = 6
TARGET_FRACTION = 0.90
MIN_TARGET_DECAY = 0.5
BLOCK_SIZE = 5
BLOCK_WORD_COUNT = 200
DAILY_NEW_TO_READ_RATIO = 1.0 / 10
BOOTSTRAP_READER_PAGE_SIZE = 1000

DATA_PATH = os.environ.get("DATA_PATH", ".")


def main():
    print("Starting up")
    with open_db() as db:
        init_db(db)

    print("Starting server")
    bottle.run(host="0.0.0.0", port=3000, debug=True)


@bottle.route("/")
def dashboard_page():
    with open_db() as db:
        word_stats = get_word_stats(db)
        day_log = get_day_log(db)
        bookmarks = get_bookmarks(db)

        bookmark_links = [
            template(
                '<a href="/practice/bootstrap?bookmark={{b}}">Bootstrap reader @{{b}}</a>',
                b=b["name"],
            )
            for b in bookmarks
        ]

        c = db.execute(
            """
            SELECT form FROM form
            WHERE decay_50 IS NULL
              AND times_seen > 0
            ORDER BY times_seen DESC
            LIMIT 10;
            """
        )
        next_by_seen = [r[0] for r in c]

        next_blocks = []
        for block in get_next_blocks(db, 8):
            next_blocks.append(
                format_template(
                    '<div><a href="{{link}}">{{name}} {{percent}}%</a></div>',
                    link=f'/practice/bootstrap?place={block["start_index"]}',
                    name=block["section_name"],
                    percent="%.1f" % (block["known_fraction"] * 100),
                )
            )
        next_blocks_html = "\n".join(next_blocks)

        progress_bar_items = [
            {
                "width": word_stats["unavailable"],
                "text": "disabled",
                "title": f'{word_stats["unavailable"]} words',
            },
            {
                "width": word_stats["available_remaining"],
                "text": "ready",
                "title": f'{word_stats["available_remaining"]} words',
            },
            {
                "width": word_stats["active"],
                "text": "active",
                "title": f'{word_stats["active"]} words',
            },
        ]

        c = db.execute(
            """
            SELECT MIN(decay_50), COUNT(*)
            FROM form
            WHERE decay_50 IS NOT NULL
            GROUP BY MIN(50, ROUND(decay_50 / 7 + 0.5) * 7);
            """
        )
        period_bar = [
            {
                "width": r[1],
                "text": f"{math.ceil(r[0])} days",
                "title": f"{r[1]} @ {math.ceil(r[0])} days",
            }
            for r in c
        ]

        c = db.execute(
            """
            SELECT ROUND(MIN(decay) * 5) / 5., COUNT(*)
            FROM (SELECT g_decay(decay_50, decay_start) AS decay
                  FROM form
                  WHERE decay_50 IS NOT NULL)
            GROUP BY ROUND(decay * 5.)
            ORDER BY decay ASC;
            """
        )
        decay_bar = [
            {"width": r[1], "text": f"{r[0]} decay", "title": f"{r[1]} @ {r[0]} decay"}
            for r in c
        ]

        c = db.execute(
            """
            SELECT text.title, form_count, known_count, start_index
            FROM block
            LEFT JOIN text ON text.id = block.section
            ORDER BY block.id ASC;
            """
        )
        rows = [r for r in c]

        blocks_html = []
        for row in rows:
            title = row[0]
            fraction = 1.0 * row[2] / row[1]
            percent = int(fraction * 100)
            level = int(fraction * fraction * 11)
            start_index = row[3]
            blocks_html.append(
                f'<a href="/practice/bootstrap?place={start_index}" class="b-{level}" title="{title} {percent}%"></a>'
            )

        block_html = '<div class="progress-blocks">' + "\n".join(blocks_html) + "</div>"

        return template(
            """
            <h1>Grozer</h1>
            <a href="/texts">Edit texts</a>
            <a href="/practice">Practice</a>
            <a href="/practice/long">Long reader</a>
            <a href="/practice/bootstrap">Bootstrap reader</a>
            <br>
            {{!bookmark_links}}

            <br>
            <form method="post" action="/new/bookmark">
                <input type="text" name="name">
                <input type="submit" value="Create bookmark">
            </form>

            <h2>Stats</h2>
            Words: {{total_words}} total; {{review_words}}/{{active_words}} review/learning; {{remaining_words}} remaining; {{unavailable_words}} unavailable.<br>

            New today: {{new_today}}; read today: {{read_today}}.<br>

            <h2>Next up</h2>
            By seen: {{next_by_seen}}<br>

            Blocks: {{!next_blocks}}<br>

            <form method="post" action="/new/words/block">
                <input type="submit" value="Add 10 words by block">
            </form>
            <br>
            <form method="post" action="/db/reindex">
                <input type="submit" value="Reindex blocks">
            </form>

            <h2>Progress</h2>
            {{!progress_bar_html}}
            {{!period_bar_html}}
            {{!decay_bar_html}}
            {{!block_html}}
            """,
            bookmark_links=" ".join(bookmark_links),
            total_words=word_stats["total"],
            review_words=word_stats["to_review"],
            active_words=word_stats["active"],
            remaining_words=word_stats["available_remaining"],
            unavailable_words=word_stats["unavailable"],
            new_today=day_log["new"],
            read_today=day_log["read"],
            next_by_seen=", ".join(next_by_seen),
            next_blocks=next_blocks_html,
            progress_bar_html=render_progress_bar(progress_bar_items),
            period_bar_html=render_progress_bar(period_bar),
            decay_bar_html=render_progress_bar(decay_bar),
            block_html=block_html,
        )


def render_progress_bar(bar_items):
    # distribute the colours evenly
    length = max(len(bar_items), 1)
    step = math.floor(11.0 / length)
    colours = [i * step + 12 - (length * step) for i in range(0, length)]

    html_items = [
        f'<span class="b-{colour}" style="flex: {item["width"]} {item["width"]};" title="{item["title"]}">{item["text"]}</span>'
        for colour, item in zip(colours, bar_items)
    ]
    return '<div class="stacked-progress">' + "\n".join(html_items) + "</div>"


@bottle.route("/texts")
def texts_page():
    with open_db() as db:
        texts = get_texts(db)

        for text in texts:
            text["known_percent"] = text["active_words"] * 100.0 / text["word_count"]
        texts.sort(key=lambda t: (t["enabled"], t["known_percent"]), reverse=True)

        texts_html = [
            template(
                """
                <p>
                    {{title}}
                    <br>

                    {{known_sentence_count}}/{{sentence_count}} sentences,
                    {{active_words}}/{{word_count}},
                    {{known_percent}}% words in cards

                    <label>
                        <input type="checkbox" name="texts" value="{{id}}" {{checked}}>
                        Enabled
                    </label>
                </p>
                """,
                id=text["id"],
                title=text["title"],
                word_count=text["word_count"],
                active_words=text["active_words"],
                sentence_count=text["sentence_count"],
                known_sentence_count=text["known_sentence_count"],
                known_percent="%.1f" % text["known_percent"],
                checked="checked" if text["enabled"] else "",
            )
            for text in texts
        ]

        return template(
            """
            <h1>Texts</h1>
            <p>
                <a href="/">Dashboard</a>
            </p>
            <h2>Examples</h2>
            <form method="post" action="/texts/edit">
                {{!body}}
                <input type="submit" value="Update texts">
            </form>
            """,
            body="\n".join(texts_html),
        )


@bottle.route("/texts/edit", method="post")
def update_texts_handler():
    params = bottle.request.forms.decode()
    enabled_ids = params.getall("texts")
    with open_db() as db:
        enable_only_texts(db, enabled_ids)
        return bottle.redirect("/texts")


@bottle.route("/practice/long")
def practice_long_read_page():
    with open_db() as db:
        long_read = find_long_read(db)
        if not long_read:
            return bottle.redirect("/practice")

        return practice_reader_template(
            db,
            long_read["translation"],
            long_read["words"],
            long_read["text_id"],
            is_long_reader=True,
        )


@bottle.route("/practice")
def practice_distributor_page():
    with open_db() as db:
        c = db.execute(
            """
            SELECT form, decay_50 FROM form
            WHERE decay_50 IS NOT NULL
            ORDER BY g_decay(decay_50, decay_start) ASC
            LIMIT 1;
            """
        )
        row = c.fetchone()
        if not row:
            return bottle.redirect("/")
        form, decay_50 = row

        # if decay_50 > 2:
        #     add_new_words_for_ratio(db)

        # check if too hard
        c = db.execute(
            """
            SELECT COUNT(*) FROM word
            JOIN sentence ON sentence.id = word.sentence_id
            JOIN sentence_stats ON sentence.id = sentence_stats.id
            WHERE word.form = ?
              AND sentence_stats.known_fraction >= ?
            """,
            (form, TARGET_FRACTION),
        )
        count = c.fetchone()[0]
        if decay_50 >= 2 and count >= 1:
            return reader_page(form)
        else:
            return practice_flashcard_page(form)

        return template("yo")


def practice_flashcard_page(form):
    with open_db() as db:
        potentials = generate_new_potentials(db, form, 4)
        card = random.choice(potentials)

        c = db.execute(
            """
            SELECT word, punctuation, form FROM word
            WHERE sentence_id = ?
            ORDER BY id;
            """,
            (card["sentence_id"],),
        )
        cloze_text = " ".join(
            w[0] + w[1] if w[2] != form else "-----" + w[1] for w in c
        )

        # get form number
        c = db.execute(
            """
            SELECT id FROM form
            WHERE form = ?;
            """,
            (form,),
        )
        form_id = c.fetchone()[0]

        # get random forms nearby in frequency list
        frequency_range = 10
        number_of_choices = 4
        c = db.execute(
            """
            SELECT form
            FROM (
                SELECT form FROM form
                WHERE id != ?
                  AND form IN (SELECT word from card)
                ORDER BY abs(id - ?)
                LIMIT ?)
            ORDER BY RANDOM()
            LIMIT ?;
            """,
            (form_id, form_id, frequency_range, number_of_choices - 1),
        )
        choices = [r[0] for r in c]
        choices.append(form)
        random.shuffle(choices)

        # get left-hand context for sentence
        c = db.execute(
            """
            SELECT text FROM sentence
            WHERE id = ? - 1
              AND book IN (SELECT book FROM sentence
                           WHERE id = ?);
            """,
            (card["sentence_id"], card["sentence_id"]),
        )
        left_context = c.fetchone()
        if left_context:
            left_context = left_context[0]

        # get right-hand context for sentence
        c = db.execute(
            """
            SELECT text FROM sentence
            WHERE id = ? + 1
              AND book IN (SELECT book FROM sentence
                           WHERE id = ?);
            """,
            (card["sentence_id"], card["sentence_id"]),
        )
        right_context = c.fetchone()
        if right_context:
            right_context = right_context[0]

        # get section name
        c = db.execute(
            """
            SELECT title FROM text
            WHERE id IN (SELECT text_id FROM sentence WHERE id = ?)
            LIMIT 1;
            """,
            (card["sentence_id"],),
        )
        section_name = c.fetchone()[0]

        practice = {
            "word": form,
            "cloze_text": cloze_text,
            "text": card["text"],
            "translation": card["translation"],
            "left_context": left_context,
            "right_context": right_context,
            "choices": choices,
        }
        practices = [practice]

        word_stats = get_word_stats(db)
        day_log = get_day_log(db)
        return practice_flashcard_page_template(
            practices,
            to_review=word_stats["to_review"],
            new_today=day_log["new"],
            read_today=day_log["read"],
            section=section_name,
        )


def practice_flashcard_page_template(
    practices, to_review, new_today, read_today, section
):
    data = {"practice": practices}

    return template(
        """
        <div class="card">
            <div class="stats">
                <a href="/">Dashboard</a>
                {{to_review}} to review;
                {{new_today}} new today;
                {{read_today}} read today
                <br>
                {{section}}
            </div>

            <div class="title"></div>
            <div class="context">
                <span class="left"></span>
                <span class="text"></span>
                <span class="right"></span>
            </div>
            <div class="translation"></div>
            <div class="choices">
                <button class="c1"></button>
                <button class="c2"></button>
                <button class="c3"></button>
                <button class="c4"></button>
            </div>

            <button class="next">Next</button>
        </div>

        <script type="application/javascript">
            window.grozerData = {{!data}}
        </script>

        <script src="/session.js"></script>
        <link rel="stylesheet" href="/session.css" type="text/css" media="all">
        """,
        data=json.dumps(data),
        section=section,
        to_review=to_review,
        new_today=new_today,
        read_today=read_today,
    )


def reader_page(word):
    with open_db() as db:
        # pick sentence
        c = db.execute(
            """
            SELECT sentence.id, sentence.text_id, translation FROM word
            JOIN sentence ON sentence.id = word.sentence_id
            JOIN sentence_stats ON sentence.id = sentence_stats.id
            WHERE word.form = ?
              AND sentence_stats.known_fraction >= ?
            ORDER BY (word_count - known_words) ASC,
                     sentence_stats.length DESC
            LIMIT ?
            """,
            (word, TARGET_FRACTION, SHORT_READER_SHUFFLE),
        )
        sentences = [{"id": r[0], "text_id": r[1], "translation": r[2]} for r in c]
        if not sentences:
            return template("no sentences")
        sentence = random.choice(sentences)

        # get words of sentence plus context
        c = db.execute(
            """
            SELECT word, punctuation, word.form, form.decay_50 IS NOT NULL, COALESCE(form.times_seen, 0) FROM word
            JOIN form ON form.form = word.form
            WHERE text_id = ?
              AND (sentence_id = ? OR sentence_id = ? - 1 OR sentence_id = ? + 1)
            ORDER BY word.id
            """,
            (sentence["text_id"], sentence["id"], sentence["id"], sentence["id"]),
        )
        words = [
            {"text": r[0] + r[1], "form": r[2], "known": r[3] == 1, "times_seen": r[4]}
            for r in c
        ]

        return practice_reader_template(
            db, sentence["translation"], words, sentence["text_id"]
        )


def practice_reader_template(db, translation, words, text_id, is_long_reader=False):
    # get section name
    c = db.execute(
        """
        SELECT title FROM text
        WHERE id = ?
        LIMIT 1;
        """,
        (text_id,),
    )
    section_name = c.fetchone()[0]

    text = []
    i = 0
    for word in words:
        text.append(
            f"""
            <span>
                <input type="hidden" name="present_words" value="{word['form']}">
                <input type="hidden" name="seen_words" value="{word['form']}">
                <input type="checkbox" id="x{i}" name="selected_words" value="{word['form']}">
                <label class="word {'known' if word['known'] else 'unknown'}" for="x{i}">
                    {word['text']}
                </label>
            </span>
            """
        )
        i += 1
    text = "\n".join(text)

    word_stats = get_word_stats(db)
    day_log = get_day_log(db)
    return template(
        """
        <h1>Reader</h1>
        <p>
            <a href="/">Dashboard</a>
            <br>
            {{to_review}} to review;
            {{new_today}} new today;
            {{read_today}} read today
            <br>
            {{section}}
        </p>
        <form autocomplete="off" method="post" action="/practice/reader/save">
            <p>
                {{!text}}
            </p>
            <p class="translation">
                {{translation}}
            </p>
            <input type="hidden" name="is_long_reader" value="{{is_long_reader}}">
            <button class="show">Show translation</button>
            <input type="submit" value="Submit">
        </form>

        <style>
        body {
            max-width: 40em;
            margin: 0 auto;
            padding: 6px;
        }

        input[type="checkbox"] { display: none; }
        .unknown { color: #888; }
        .word:hover, .word:focus { background-color: #CCC; }

        input:checked ~ .word.known {
            color: red;
            text-decoration: underline;
        }
        input:checked ~ .word.unknown {
            color: green;
            text-decoration: underline;
        }
        </style>

        <script type="text/javascript">
        const translationEl = document.querySelector('.translation')
        const submitEl = document.querySelector('[type="submit"]')

        translationEl.style = 'visibility: hidden'
        submitEl.style = 'visibility: hidden'

        const showBtn = document.querySelector('.show')
        showBtn.addEventListener('click', ev => {
            ev.preventDefault()
            translationEl.style = ''
            submitEl.style = ''
        })
        </script>
        """,
        text=text,
        translation=translation,
        to_review=word_stats["to_review"],
        new_today=day_log["new"],
        read_today=day_log["read"],
        section=section_name,
        is_long_reader="true" if is_long_reader else "false",
    )


@bottle.route("/practice/bootstrap")
def practice_bootstrap_page():
    with open_db() as db:
        bookmark_name = bottle.request.query.get("bookmark")
        place = bottle.request.query.get("place")
        if place is not None:
            place = int(place)
        if bookmark_name:
            bookmark = get_bookmark(db, bookmark_name)
        else:
            bookmark = None

        long_read = find_bootstrap_read(db, place, bookmark)
        if not long_read:
            return bottle.redirect("/practice")

        return practice_bootstrap_template(
            db,
            long_read["words"],
            long_read["text_id"],
            long_read["place"],
            bookmark,
        )


def find_bootstrap_read(db, place=None, bookmark=None):
    if place:
        start_index = place
    elif bookmark:
        start_index = bookmark["word_index"]
    else:
        start_index = 1

    if not isinstance(start_index, int):
        start_index = 1

    length = BOOTSTRAP_READER_PAGE_SIZE

    # find sentences in the region
    c = db.execute(
        """
        SELECT id, start_word, end_word, text_id, book, translation
        FROM sentence
        WHERE sentence.start_word < ?
          AND sentence.end_word > ?
        ORDER BY sentence.id ASC
        """,
        (start_index + length, start_index),
    )
    sentences = [
        {
            "id": r[0],
            "start": r[1],
            "end": r[2],
            "text_id": r[3],
            "book": r[4],
            "translation": r[5],
        }
        for r in c
    ]
    if not sentences:
        return template("no sentences")
    sentences_start = min(s["start"] for s in sentences)
    sentences_end = max(s["end"] for s in sentences)

    # get words of sentences
    c = db.execute(
        """
        SELECT word || punctuation,
               word.form,
               word.interlinear,
               form.frequency,
               form.decay_50 IS NOT NULL,
               COALESCE(form.times_seen, 0),
               word.paragraph
        FROM word
        JOIN form ON form.form = word.form
        WHERE word.id >= ?
          AND word.id <= ?
          AND word.book = ?
        ORDER BY word.id
        """,
        (sentences_start, sentences_end, sentences[0]["book"]),
    )
    words = [
        {
            "text": r[0],
            "form": r[1],
            "interlinear": r[2],
            "freq": r[3],
            "known": r[4] == 1,
            "times_seen": r[5],
            "ends_paragraph": r[6] == 1,
        }
        for r in c
    ]

    return {
        "words": words,
        "text_id": sentences[0]["text_id"],
        "place": start_index,
    }


def practice_bootstrap_template(db, words, text_id, place, bookmark):
    # get section name
    c = db.execute(
        """
        SELECT title FROM text
        WHERE id = ?
        LIMIT 1;
        """,
        (text_id,),
    )
    section_name = c.fetchone()[0]

    # get previous book
    c = db.execute(
        """
        SELECT word.id, text.title FROM word
        JOIN text ON text.id = word.text_id
        WHERE book = (SELECT book - 1 FROM word
                      WHERE id = ?)
        ORDER BY word.id ASC
        LIMIT 1;
        """,
        (place,),
    )
    previous_book = c.fetchone()

    # get next book
    c = db.execute(
        """
        SELECT word.id, text.title FROM word
        JOIN text ON text.id = word.text_id
        WHERE book = (SELECT book + 1 FROM word
                      WHERE id = ?)
        ORDER BY word.id ASC
        LIMIT 1;
        """,
        (place,),
    )
    next_book = c.fetchone()

    previous_place = max(0, place - BOOTSTRAP_READER_PAGE_SIZE)
    next_place = place + min(BOOTSTRAP_READER_PAGE_SIZE, len(words))

    # find top N unknown by Freq
    num_new_words = math.ceil(len(words) * (1 - 0.98))
    by_freq = {w["form"]: w["freq"] for w in words if not w["known"]}
    by_freq = list(by_freq.items())
    by_freq.sort(key=lambda w: -w[1])
    by_freq = by_freq[0:num_new_words]
    to_show = set(w[0] for w in by_freq)

    text = []
    i = 0
    num_known = 0
    num_learning = 0
    for word in words:
        mode = "unknown"
        if word["known"]:
            mode = "known"
            num_known += 1
        elif word["form"] in to_show:
            mode = "learn"
            num_learning += 1

        seen = "not_" if mode == "unknown" else ""

        paragraph_break = "</p><p>" if word["ends_paragraph"] else ""

        text.append(
            f"""
            <span>
                <input type="hidden" name="present_words" value="{word['form']}">
                <input type="hidden" name="{seen}seen_words" value="{word['form']}">
                <input type="checkbox" id="x{i}" name="selected_words" value="{word['form']}">
                <label class="word {mode}" for="x{i}">
                    <span class="word_native">{word['text']}</span>
                    <span class="word_translation">{word['interlinear']}</span>
                </label>
            </span>
            {paragraph_break}
            """
        )
        i += 1
    text = "\n".join(text)

    if bookmark:
        bookmark_vars = format_template(
            '<input type="hidden" name="bookmark" value="{{name}}">',
            name=bookmark["name"],
        )
    else:
        bookmark_vars = ""

    def reader_link(to, text):
        return format_template(
            '<a href="/practice/bootstrap{{to}}">{{text}}</a>', to=to, text=text
        )

    if bookmark:
        bookmark_q = f"&bookmark={bookmark['name']}"
    else:
        bookmark_q = ""
    section_links = [
        reader_link(f"?place={previous_place}{bookmark_q}", "< Prev"),
        reader_link(f"?place={place}", section_name),
        reader_link(f"?place={next_place}{bookmark_q}", "Next >"),
    ]
    if previous_book:
        section_links.insert(
            0,
            reader_link(
                f"?place={previous_book[0]}{bookmark_q}", f"<< {previous_book[1]}"
            ),
        )
    if next_book:
        section_links.append(
            reader_link(f"?place={next_book[0]}{bookmark_q}", f"{next_book[1]} >>")
        )

    word_stats = get_word_stats(db)
    day_log = get_day_log(db)
    return template(
        """
        <h1>Bootstrap Reader</h1>
        <p>
            <a href="/">Dashboard</a>
            <br>
            {{to_review}} to review;
            {{new_today}} new today;
            {{read_today}} read today
            <br>
            {{!section_links}}
            <br>
            {{known_percent}}% known; {{num_learning}} new ({{num_learning_unique}} unique)
        </p>
        <form autocomplete="off" method="post" action="/practice/reader/save">
            <p>
                {{!text}}
            </p>
            <input type="hidden" name="prev_place" value="{{previous_place}}">
            <input type="hidden" name="next_place" value="{{next_place}}">
            {{!bookmark_vars}}
            <input type="submit" name="goto" value="Previous">
            <input type="submit" name="goto" value="Next">
        </form>

        <style>
        body {
            max-width: 40em;
            margin: 0 auto;
            padding: 6px;
            font-size: 20px;
        }

        @media only screen and (min-width: 960px) {
            body {
                font-size: 24px;
            }
        }

        form {
            margin-bottom: 24px;
        }

        p {
            text-align: justify;
        }

        input[type="checkbox"] { display: none; }
        .word:hover, .word:focus { background-color: #CCC; }

        .word {
            vertical-align: top;
            line-height: 1.8;
        }
        .word.learn {
            text-decoration: underline;
        }
        .word .word_translation {
            color: #444;
        }

        .word .word_translation::before {
            content: '{';
        }
        .word .word_translation::after {
            content: '}';
        }
        .word.unknown .word_translation::before,
        .word.unknown .word_translation::after {
            content: initial;
        }

        .word.known .word_translation {
            display: none;
        }
        input:checked ~ .word.known {
            color: red;
            text-decoration: underline;
        }
        input:checked ~ .word.known .word_translation {
            display: initial;
        }

        .word.unknown .word_native {
            display: none;
        }
        input:checked ~ .word.unknown,
        input:checked ~ .word.learn {
            color: green;
            text-decoration: underline;
        }
        input:checked ~ .word.unknown .word_native {
            display: initial;
        }
        </style>
        """,
        text=text,
        previous_place=previous_place,
        place=place,
        next_place=next_place,
        section_links=" ".join(section_links),
        bookmark_vars=bookmark_vars,
        to_review=word_stats["to_review"],
        new_today=day_log["new"],
        read_today=day_log["read"],
        section=section_name,
        known_percent="%.1f" % (num_known * 100.0 / len(words)),
        num_learning=num_learning,
        num_learning_unique=num_new_words,
    )


@bottle.route("/practice/reader/save", method="post")
def save_reader_handler():
    params = bottle.request.forms.decode()
    words_present = params.getall("present_words")
    words_seen = params.getall("seen_words")
    words_selected = params.getall("selected_words")
    is_long_reader = params.get("is_long_reader") == "true"

    if "goto" in params:
        if params["goto"] == "Previous":
            place = int(params["prev_place"])
        else:
            place = int(params["next_place"])
    else:
        place = None
    bookmark = params.get("bookmark")

    with open_db() as db:
        for word in words_seen:
            db.execute(
                """
                UPDATE form SET times_seen = COALESCE(times_seen, 0) + 1,
                                last_seen = ?
                WHERE form = ?;
                """,
                (now_dt().isoformat(), word),
            )
        update_log(db, read_words=len(words_seen))

        words_to_add = set()
        known_words = {}
        for word in words_present:
            c = db.execute(
                """
                SELECT decay_50, decay_start FROM form
                WHERE decay_50 IS NOT NULL
                  AND form = ?;
                """,
                (word,),
            )
            row = c.fetchone()

            if row:
                known_words[word] = {"rate_50": row[0], "decay_start": row[1]}
            elif word in words_selected:
                words_to_add.add(word)

        for word, state in known_words.items():
            word_was_good = word not in words_selected
            if word_was_good:
                next_rate_50 = continue_decay(state["rate_50"], state["decay_start"])
            else:
                next_rate_50 = state["rate_50"] / 2

            db.execute(
                """
                UPDATE form SET decay_50 = ?,
                                decay_start = ?
                WHERE form = ?;
                """,
                (next_rate_50, now_dt().isoformat(), word),
            )

        for word in words_to_add:
            rate_50 = timedelta(minutes=10) / timedelta(days=1)
            db.execute(
                """
                UPDATE form SET decay_50 = ?,
                                decay_start = ?
                WHERE form = ?;
                """,
                (rate_50, now_dt().isoformat(), word),
            )
            update_log(db, new_words=1)

        if bookmark is not None and place is not None:
            update_bookmark(db, bookmark, place)

        reindex_blocks(db, use_txn=False)
        db.commit()

        if bookmark is not None:
            return bottle.redirect(f"/practice/bootstrap?bookmark={bookmark}")
        elif place is not None:
            return bottle.redirect(f"/practice/bootstrap?place={place}")
        elif is_long_reader:
            return bottle.redirect("/practice/long")
        else:
            return bottle.redirect("/practice")


@bottle.route("/new/words/freq", method="post")
def add_words_freq_handler():
    with open_db() as db:
        c = db.execute(
            """
            SELECT form FROM form
            WHERE decay_50 IS NULL
              AND form IN (SELECT form
                           FROM word
                           JOIN text ON text.id = word.text_id
                                    AND text.enabled = 1)
            ORDER BY frequency DESC
            LIMIT 5;
            """
        )
        words_to_add = [r[0] for r in c]

        for word in words_to_add:
            rate_50 = timedelta(minutes=10) / timedelta(days=1)
            db.execute(
                """
                UPDATE form SET decay_50 = ?,
                                decay_start = ?
                WHERE form = ?;
                """,
                (rate_50, now_dt().isoformat(), word),
            )
            update_log(db, new_words=1)

        reindex_blocks(db, use_txn=False)
        db.commit()

        return bottle.redirect("/")


@bottle.route("/new/words/block", method="post")
def add_words_block_handler():
    with open_db() as db:
        for i in range(0, 10):
            words_to_add = get_next_by_block(db)
            if not words_to_add:
                break

            word = words_to_add[0]
            rate_50 = timedelta(minutes=10) / timedelta(days=1)
            db.execute(
                """
                UPDATE form SET decay_50 = ?,
                                decay_start = ?
                WHERE form = ?;
                """,
                (rate_50, now_dt().isoformat(), word),
            )
            update_log(db, new_words=1)
            reindex_blocks(db, use_txn=False)

        db.commit()

        return bottle.redirect("/")


@bottle.route("/db/reindex", method="post")
def reindex_blocks_handler():
    with open_db() as db:
        reindex_blocks(db, use_txn=True, rebuild=True)
        return bottle.redirect("/")


@bottle.route("/new/bookmark", method="post")
def reindex_blocks_handler():
    params = bottle.request.forms.decode()
    name = params.get("name")
    with open_db() as db:
        if name:
            create_bookmark(db, name)
            db.commit()
        return bottle.redirect("/")


@bottle.route("/session.js")
def asset_session_js():
    return bottle.static_file(
        "/session.js", root=os.path.dirname(__file__), mimetype="application/javascript"
    )


@bottle.route("/session.css")
def asset_session_css():
    return bottle.static_file(
        "/session.css", root=os.path.dirname(__file__), mimetype="text/css"
    )


@bottle.route("/api/card/<form>/<correct>", method="post")
def update_practice_route(form, correct):
    with open_db() as db:
        c = db.execute(
            """
            SELECT decay_50, decay_start FROM form
            WHERE decay_50 IS NOT NULL
              AND form = ?;
            """,
            (form,),
        )
        row = c.fetchone()
        if not row:
            return bottle.redirect("/")

        rate_50, decay_start = row[0], row[1]
        if correct == "true":
            next_rate_50 = continue_decay(rate_50, decay_start)
        else:
            next_rate_50 = rate_50 / 2

        db.execute(
            """
            UPDATE form SET decay_50 = ?,
                            decay_start = ?,
                            times_seen = COALESCE(times_seen, 0) + 1
            WHERE form = ?;
            """,
            (next_rate_50, now_dt().isoformat(), form),
        )

        update_log(db, read_words=1)
        reindex_blocks(db, use_txn=False)
        db.commit()


def format_template(content, **kwargs):
    return bottle.template(content, **kwargs)


def template(content, **kwargs):
    content = format_template(content, **kwargs)
    return bottle.template(
        """
        <!doctype html>
        <html>
        <head>
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <title>Grozer</title>
        </head>
        <body>
            {{!content}}

            <style>
            .stacked-progress {
                margin: 12px 0;
                max-width: 680px;
                border-radius: 3px;
                overflow: hidden;
                display: flex;
            }
            .stacked-progress span {
                min-width: 12px;
                padding: 12px 6px;
                overflow: hidden;
                white-space: nowrap;
            }

            .progress-blocks {
                margin: 12px 0;
                max-width: 680px;
                border-radius: 3px;
                overflow: hidden;
                display: flex;
                flex-wrap: wrap;
            }
            .progress-blocks a {
                flex: 1 0 auto;
                width: 15px;
                height: 15px;
            }

            .b-0 {
                background-color: #240036;
                color: #FFFFFF;
            }
            .b-1 {
                background-color: #27013a;
                color: #FFFFFF;
            }
            .b-2 {
                background-color: #2e0544;
                color: #FFFFFF;
            }
            .b-3 {
                background-color: #390d54;
                color: #FFFFFF;
            }
            .b-4 {
                background-color: #481a67;
                color: #FFFFFF;
            }
            .b-5 {
                background-color: #5b2d7c;
                color: #FFFFFF;
            }
            .b-6 {
                background-color: #704593;
                color: #FFFFFF;
            }
            .b-7 {
                background-color: #8661a8;
                color: #FFFFFF;
            }
            .b-8 {
                background-color: #9d7fbb;
            }
            .b-9 {
                background-color: #b19acb;
            }
            .b-10 {
                background-color: #c0add5;
            }
            .b-11 {
                background-color: #c5b4d9;
            }
            </style>
        </body>
        </html>
        """.strip(),
        content=content,
    )


def open_db():
    db_path = os.path.join(DATA_PATH, "main.db")
    db = sqlite3.connect(db_path)
    try:
        db.create_function("g_decay", 2, decay_fn)
        return db
    except Exception as e:
        db.close()
        raise e


def init_db(db):
    db.execute(
        """
        CREATE TABLE IF NOT EXISTS card (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            -- set created_at to "now", in UTC
            created_at TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%S', 'now')),
            sentence_id INT,
            word TEXT,
            cloze_text TEXT,
            text TEXT,
            translation TEXT,
            review_period_days INT,
            next_review TEXT
        );
        """
    )

    db.execute("""CREATE INDEX IF NOT EXISTS card_sentence_id ON card (sentence_id);""")
    db.execute("""CREATE INDEX IF NOT EXISTS card_word ON card (word);""")
    db.execute("""CREATE INDEX IF NOT EXISTS card_next_review ON card (next_review);""")

    db.execute(
        """
        CREATE TABLE IF NOT EXISTS review (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            -- set reviewed_at to "now", in UTC
            reviewed_at TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%S', 'now')),
            card_id INTEGER,
            was_correct BOOLEAN
        );
        """
    )

    db.execute("""CREATE INDEX IF NOT EXISTS review_card_id ON review (card_id);""")
    db.execute(
        """CREATE INDEX IF NOT EXISTS review_reviewed_at ON review (reviewed_at);"""
    )

    db.execute(
        """
        CREATE TABLE IF NOT EXISTS day_log (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            date TEXT UNIQUE NOT NULL,
            new_count INTEGER NOT NULL,
            read_count INTEGER NOT NULL
        );
        """
    )

    db.execute(
        """
        CREATE TABLE IF NOT EXISTS block (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            text INTEGER NOT NULL REFERENCES word(book),
            section INTEGER NOT NULL REFERENCES text(id),
            start_index INTEGER NOT NULL REFERENCES word(id),
            end_index INTEGER NOT NULL REFERENCES word(id),
            form_count INTEGER NOT NULL,
            known_count INTEGER NOT NULL
        );
        """
    )
    db.execute(
        """
        CREATE INDEX IF NOT EXISTS block_known_fraction
        ON block ((1. * known_count) / form_count);
        """
    )

    # a view to get known forms
    db.execute("DROP VIEW IF EXISTS known_form;")
    db.execute(
        """
        CREATE VIEW IF NOT EXISTS known_form (form) AS
        SELECT form FROM form WHERE decay_50 IS NOT NULL;
        """
    )

    # a view of sentences to known word counts
    db.execute("DROP VIEW IF EXISTS sentence_known_words;")
    db.execute(
        """
        CREATE VIEW IF NOT EXISTS sentence_known_words AS
        SELECT sentence_id AS id,
               COUNT(DISTINCT form) as known_words
        FROM word
        WHERE form IN (SELECT form FROM known_form)
        GROUP BY sentence_id;
        """
    )

    # a view of sentences to unknown word counts
    db.execute("DROP VIEW IF EXISTS sentence_unknown_words;")
    db.execute(
        """
        CREATE VIEW IF NOT EXISTS sentence_unknown_words AS
        SELECT sentence_id AS id,
               COUNT(DISTINCT form) as unknown_words
        FROM word
        WHERE form NOT IN (SELECT form FROM known_form)
        GROUP BY sentence_id;
        """
    )

    # a view of sentences with stats
    db.execute("DROP VIEW IF EXISTS sentence_stats;")
    db.execute(
        """
        CREATE VIEW IF NOT EXISTS sentence_stats AS
        SELECT id, text_id, word_count, known_words, unknown_words,
               length,
               (known_words * 1.0) / word_count AS known_fraction
        FROM (
            SELECT sentence.id, sentence.text_id,
                   (end_word - start_word + 1) AS word_count,
                   COALESCE(known_words, 0) AS known_words,
                   COALESCE(unknown_words, 0) AS unknown_words,
                   length(text) AS length
            FROM sentence
            LEFT JOIN sentence_known_words
                   ON sentence.id = sentence_known_words.id
            LEFT JOIN sentence_unknown_words
                   ON sentence.id = sentence_unknown_words.id);
        """
    )

    db.execute(
        """
        CREATE TABLE IF NOT EXISTS bookmark (
            name TEXT PRIMARY KEY,
            -- set created_at to "now", in UTC
            created_at TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%S', 'now')),
            updated_at TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%S', 'now')),
            word_index INTEGER NOT NULL REFERENCES word(id)
        );
        """
    )
    db.execute(
        "CREATE INDEX IF NOT EXISTS bookmark_updated_at ON bookmark (updated_at);"
    )
    db.execute("INSERT OR IGNORE INTO bookmark (name, word_index) VALUES ('main', 1);")

    db.commit()


def add_new_words_for_ratio(db):
    # probability of getting a new word is based on the review queue length
    word_stats = get_word_stats(db)
    if word_stats["to_review"] <= 0:
        chance = 0.8
    elif word_stats["to_review"] <= 20:
        chance = 0.5
    elif word_stats["to_review"] <= 50:
        chance = 0.1
    elif word_stats["to_review"] <= 200:
        chance = 0.02
    else:
        chance = 0

    if random.random() > chance:
        return

    words_to_add = get_next_by_block(db)
    if not words_to_add:
        return

    word = words_to_add[0]
    rate_50 = timedelta(minutes=10) / timedelta(days=1)
    db.execute(
        """
        UPDATE form SET decay_50 = ?,
                        decay_start = ?
        WHERE form = ?;
        """,
        (rate_50, now_dt().isoformat(), word),
    )
    update_log(db, new_words=1)

    reindex_blocks(db, use_txn=False)
    db.commit()


def get_word_stats(db):
    c = db.execute("SELECT COUNT(*) FROM form;")
    forms = c.fetchone()[0]

    c = db.execute("SELECT COUNT(*) FROM form WHERE decay_50 IS NOT NULL;")
    active = c.fetchone()[0]

    c = db.execute(
        """
        SELECT COUNT(*), COUNT(decay_50)
        FROM form
        WHERE form IN (SELECT form
                       FROM word
                       JOIN text ON text.id = word.text_id
                                AND text.enabled = 1);
        """
    )
    available, available_active = c.fetchone()

    c = db.execute(
        """
        SELECT COUNT(*) FROM form
        WHERE decay_50 IS NOT NULL
          AND g_decay(decay_50, decay_start) < ?;
        """,
        (MIN_TARGET_DECAY,),
    )
    to_review = c.fetchone()[0]

    return {
        "total": forms,
        "active": active,
        "available": available,
        "available_remaining": available - available_active,
        "unavailable": forms - available,
        "to_review": to_review,
    }


def get_texts(db, limit=1_000_000):
    c = db.execute(
        """
        SELECT id, title, enabled,
               sentence_count.count,
               known_sentence_count.count,
               word_count.count,
               active_count.count
        FROM text
        LEFT JOIN (SELECT text_id, COUNT(*) AS count
                   FROM sentence
                   GROUP BY text_id)
             AS sentence_count
             ON sentence_count.text_id = text.id
        LEFT JOIN (SELECT text_id, COUNT(*) AS count
                   FROM sentence_stats
                   WHERE unknown_words = 0
                   GROUP BY text_id)
             AS known_sentence_count
             ON known_sentence_count.text_id = text.id
        LEFT JOIN (SELECT text_id, COUNT(DISTINCT form) AS count
                   FROM word
                   GROUP BY text_id)
             AS word_count
             ON word_count.text_id = text.id
        LEFT JOIN (SELECT text_id, COUNT(DISTINCT form) AS count
                   FROM word
                   WHERE form IN (SELECT form FROM form
                                  WHERE decay_50 IS NOT NULL)
                   GROUP BY text_id)
             AS active_count
             ON active_count.text_id = text.id
        LIMIT ?;
        """,
        (limit,),
    )
    return [
        {
            "id": r[0],
            "title": r[1],
            "enabled": r[2] > 0,
            "sentence_count": r[3],
            "known_sentence_count": r[4],
            "word_count": r[5],
            "active_words": r[6],
        }
        for r in c
    ]


def generate_new_potentials(db, form, limit):
    c = db.execute(
        """
        SELECT sentence.id, word.form, text, translation FROM word
        JOIN sentence ON sentence.id = word.sentence_id
        JOIN sentence_stats ON sentence.id = sentence_stats.id
        WHERE word.form = ?
        ORDER BY (word_count - known_words) ASC,
                 sentence_stats.length ASC
        LIMIT ?
        """,
        (form, limit),
    )

    return [
        {"sentence_id": r[0], "word": r[1], "text": r[2], "translation": r[3]}
        for r in c
    ]


def get_next_blocks(db, limit):
    c = db.execute(
        """
        SELECT block.id,
               text.title,
               block.start_index,
               block.end_index,
               (1. * known_count) / form_count AS kf
        FROM block
        JOIN text ON text.id = block.section
        WHERE kf < ?
          AND text.enabled
        ORDER BY kf DESC,
                 block.start_index ASC
        LIMIT ?;
        """,
        (TARGET_FRACTION, limit),
    )

    def map(row):
        return {
            "id": row[0],
            "section_name": row[1],
            "start_index": row[2],
            "end_index": row[3],
            "known_fraction": row[4],
        }

    return [map(r) for r in c]


def get_next_by_block(db):
    blocks = get_next_blocks(db, 1)
    if not blocks:
        return []
    block = blocks[0]

    # find best words by frequency in block
    c = db.execute(
        """
        SELECT form
        FROM form
        WHERE form IN (SELECT form
                       FROM word
                       WHERE id >= ? AND id <= ?)
          AND decay_50 IS NULL
        ORDER BY frequency DESC
        LIMIT 5;
        """,
        (block["start_index"], block["end_index"]),
    )
    return [r[0] for r in c]


def find_long_read(db):
    # find a block that is readable (that's the subquery), and pick the most
    # decayed
    c = db.execute(
        """
        SELECT block.start_index, block.end_index
        FROM block
        LEFT JOIN word w
               ON w.id >= block.start_index
              AND w.id <= block.end_index
        LEFT JOIN form f
               ON f.form = w.form
        WHERE (1. * known_count) / form_count > ?
        GROUP BY block.start_index
        ORDER BY SUM(
                   g_decay(f.decay_50, f.decay_start)
                   * g_decay(f.decay_50, f.decay_start))
                 / COUNT(f.decay_50) ASC
        LIMIT 1;
        """,
        (TARGET_FRACTION,),
    )
    row = c.fetchone()
    if not row:
        return None
    block_start, block_end = row

    # find sentences that overlap
    c = db.execute(
        """
        SELECT id, start_word, end_word, text_id, translation
        FROM sentence
        WHERE sentence.start_word < ?
          AND sentence.end_word > ?
        ORDER BY sentence.id ASC
        """,
        (block_end, block_start),
    )
    sentences = [
        {"id": r[0], "start": r[1], "end": r[2], "text_id": r[3], "translation": r[4]}
        for r in c
    ]
    if not sentences:
        return template("no sentences")
    sentences_start = min(s["start"] for s in sentences)
    sentences_end = max(s["end"] for s in sentences)

    # get words of sentences
    c = db.execute(
        """
        SELECT word || punctuation, word.form, form.decay_50 IS NOT NULL, COALESCE(form.times_seen, 0)
        FROM word
        JOIN form ON form.form = word.form
        WHERE word.id >= ?
          AND word.id <= ?
        ORDER BY word.id
        """,
        (sentences_start, sentences_end),
    )
    words = [
        {"text": r[0], "form": r[1], "known": r[2] == 1, "times_seen": r[3]} for r in c
    ]

    return {
        "words": words,
        "translation": " ".join(s["translation"] for s in sentences),
        "text_id": sentences[0]["text_id"],
    }


def enable_only_texts(db, enabled_ids):
    db.execute("BEGIN")

    db.execute("UPDATE text SET enabled = 0;")
    db.execute(
        f"""
        UPDATE text SET enabled = 1
        WHERE id IN ({', '.join(enabled_ids)});
        """
    )

    db.commit()


def update_log(db, new_words=0, read_words=0):
    now = now_dt_local_tz()
    day = now.strftime("%Y-%m-%d")

    if not db.in_transaction:
        db.execute("BEGIN")
    try:
        db.execute(
            """
            INSERT INTO day_log (date, new_count, read_count)
            VALUES (?, ?, ?);
            """,
            (day, new_words, read_words),
        )
    except sqlite3.IntegrityError:
        db.execute(
            """
            UPDATE day_log
            SET new_count = new_count + ?,
                read_count = read_count + ?
            WHERE date = ?;
            """,
            (new_words, read_words, day),
        )
    if not db.in_transaction:
        db.commit()


def get_day_log(db):
    now = now_dt_local_tz()
    day = now.strftime("%Y-%m-%d")

    c = db.execute(
        """
        SELECT new_count, read_count
        FROM day_log
        WHERE date = ?
        LIMIT 1;
        """,
        (day,),
    )
    row = c.fetchone()
    if not row:
        return {"new": 0, "read": 0}
    else:
        return {"new": row[0], "read": row[1]}


def decay_fn(rate_50, decay_start):
    """
    An exponential decay function, operating on a start date and number of
    days to reach a decay of 0.5.
    """

    if not rate_50 or not decay_start:
        return 0.0

    # sort new cards so they appear first
    if rate_50 < 1:
        return 0.0

    tz = pytz.timezone("Australia/Adelaide")
    decay_start = datetime.fromisoformat(decay_start)
    decay_start = decay_start.astimezone(tz)
    now = datetime.now(tz)
    delta = now - decay_start
    delta_days = delta / timedelta(days=1)

    h = -math.log(0.2) / rate_50
    y = math.exp(-h * delta_days)

    return y


def continue_decay(rate_50, decay_start):
    """
    Calculates the "next" rate_50, the one needed to keep the same derivative as
    the decay_start.
    """

    if rate_50 < 1:
        # take boost new cards through the process
        return rate_50 + 5.8

    h_before = -math.log(0.2) / rate_50

    y_before = decay_fn(rate_50, decay_start)
    # clamp this so overdue review words don't get massive bonuses
    y_before = max(0.3, y_before)

    derivative = -h_before * y_before
    h_after = -derivative
    rate_50_after = -math.log(0.2) / h_after

    # don't let the interval go longer than a year
    rate_50_after = min(365, rate_50_after)

    return rate_50_after


def now_dt():
    return datetime.now(pytz.utc)


def now_dt_local_tz():
    tz = pytz.timezone("Australia/Adelaide")
    return datetime.now(tz)


def reindex_blocks(db, use_txn, rebuild=False):
    if use_txn:
        db.execute("BEGIN")

    if rebuild:
        # clear table
        db.execute("""DELETE FROM block;""")

        # create block entries
        for book, start, length in db.execute(
            "SELECT book, MIN(id), COUNT(*) FROM word GROUP BY book;"
        ):
            index = start
            while index < start + length:
                block_end = min(index + BLOCK_WORD_COUNT, start + length - 1)

                db.execute(
                    """
                    INSERT INTO block (text, section,
                                       start_index, end_index,
                                       form_count, known_count)
                    VALUES (?, 1, ?, ?, 0, 0);
                    """,
                    (book, index, block_end),
                )

                if block_end - index < BLOCK_WORD_COUNT:
                    break
                index += int(BLOCK_WORD_COUNT / 4.0 * 3)

        # update block sections
        db.execute(
            """
            UPDATE block
            SET section = (
                SELECT text_id
                FROM word
                WHERE word.id = block.start_index);
            """
        )

    # calculate block stats
    db.execute(
        """
        UPDATE block
        SET (form_count, known_count) = (
            SELECT COUNT(*), COUNT(decay_50)
            FROM form
            WHERE form IN (SELECT form FROM word
                           WHERE id >= block.start_index
                             AND id <= block.end_index));
        """
    )

    if use_txn:
        db.commit()


def get_bookmarks(db):
    c = db.execute(
        """
        SELECT name, created_at, updated_at, word_index
        FROM bookmark
        ORDER BY updated_at DESC;
        """,
    )
    return [
        {
            "name": row[0],
            "created_at": row[1],
            "updated_at": row[2],
            "word_index": row[3],
        }
        for row in c
    ]


def get_bookmark(db, name):
    c = db.execute(
        """
        SELECT name, created_at, updated_at, word_index
        FROM bookmark
        WHERE name = ?;
        """,
        (name,),
    )
    row = c.fetchone()
    if row:
        return {
            "name": row[0],
            "created_at": row[1],
            "updated_at": row[2],
            "word_index": row[3],
        }
    else:
        return None


def create_bookmark(db, name):
    db.execute("INSERT INTO bookmark (name, word_index) VALUES (?, 1);", (name,))


def update_bookmark(db, name, place):
    db.execute(
        """
        UPDATE bookmark
        SET word_index = ?,
            updated_at = strftime('%Y-%m-%dT%H:%M:%S', 'now')
        WHERE name = ?;
        """,
        (place, name),
    )


main()
